#pragma once

#include <units/voltage.h>
#include <units/current.h>
#include "thunder/Tuneable.h"

namespace constants{
namespace amp{
    constexpr int kPivotMotorPort = 11;
    constexpr units::ampere_t kContinuousCurrentLimit = 15.0_A;
    constexpr units::ampere_t kPeakCurrentLimit = 20.0_A;
}

}