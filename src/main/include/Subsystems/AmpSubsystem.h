#pragma once

#include <frc2/command/InstantCommand.h>
#include <frc2/command/SubsystemBase.h>
#include <frc2/command/CommandPtr.h>
#include <frc2/command/RunCommand.h>
#include <units/voltage.h>

#include <ctre/phoenix/motorcontrol/can/WPI_TalonSRX.h>
#include "Constants.h"

class AmpSubsystem : public frc2::SubsystemBase{
    public:
        AmpSubsystem();

        void Periodic() override;

        auto RunWithVoltage(units::volt_t voltage) -> frc2::CommandPtr;

    private:
        ctre::phoenix::motorcontrol::can::WPI_TalonSRX m_pivot_motor{constants::amp::kPivotMotorPort};
};