#include "Subsystems/AmpSubsystem.h"
#include <iostream>

AmpSubsystem::AmpSubsystem() {
    m_pivot_motor.ConfigFactoryDefault();

    m_pivot_motor.ConfigNominalOutputForward(0);
    m_pivot_motor.ConfigNominalOutputReverse(0);
    m_pivot_motor.ConfigPeakOutputForward(1);
    m_pivot_motor.ConfigPeakOutputReverse(-1);


    m_pivot_motor.EnableCurrentLimit(true);
    m_pivot_motor.ConfigPeakCurrentLimit(static_cast<units::ampere_t>(constants::amp::kPeakCurrentLimit).value());
    m_pivot_motor.ConfigContinuousCurrentLimit(static_cast<units::ampere_t>(constants::amp::kContinuousCurrentLimit).value());

    m_pivot_motor.ConfigSelectedFeedbackSensor(ctre::phoenix::motorcontrol::FeedbackDevice::CTRE_MagEncoder_Relative, 0);

    std::cout << "configured";

};

auto AmpSubsystem::RunWithVoltage (units::volt_t voltage) -> frc2::CommandPtr {
        std::cout << "running amp at " << voltage.value() << "V" << std::endl;
        return frc2::RunCommand(
            [&] {m_pivot_motor.SetVoltage(voltage);}, {this}
            ).ToPtr();
}
